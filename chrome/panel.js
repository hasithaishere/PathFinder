var app = angular.module('MyApp',['ngMaterial', 'ngMessages', 'material.svgAssetsCache', 'ngRoute', 'angular-clipboard']);

app.config(function($routeProvider){
    $routeProvider
      .when('/',{
          templateUrl: 'pages/signin.template.html',
          controller : 'signinCtrl'
      })
      .when('/workbench',{
          templateUrl: 'pages/workbench.template.html',
          controller : 'workbenchCtrl'
      })
      .otherwise({ 
        template: '<h1>Not Found</h1>'
      });
});

app.service('inscpectionService', function ($rootScope) {
  $rootScope.$broadcast('element-broadcaster');
  
  this.bindElementListener = function () {
    let updateSelectedElement = function () {
      function extractNodeData() {
        /*
        * Handle the heighlight functionality for selected dom element.
        */
        let highlightElement = function (node) {
          //Set border for highlight the selected element.
          node.style['box-shadow'] = "0px 0px 61px 5px #3fa6f4";

          // Remove border from highlighted element after specific time period.
          setTimeout(function() { 
            node.style['box-shadow'] = "none"; 
          }, 2000);
        };

        highlightElement($0);
        
        let generateElementTitle = function (node) {
          let allowedTitleAttributes = ['value', 'placeholder', 'title', 'name', 'aria-label', 'alt'];
          let content = null;
          let elementTitle = node.nodeName || null;

          allowedTitleAttributes.some(function (attribute) {
            if(node.hasAttribute(attribute) && node.getAttribute(attribute) !== ''){
              content = node.getAttribute(attribute).trim();
              return true;
            }
            return false;
          });

          if (content === null && node.textContent !== '' && node.nodeName !== 'SELECT') {

            content = node.textContent.trim();

            let MAX_CONTENT_LENGTH = 20;

            if (content.length > MAX_CONTENT_LENGTH) {
              content = content.slice(0, MAX_CONTENT_LENGTH) + '...';
            }
          }

          if (content !== null) {
            elementTitle += (" : " + content);
          }

          return elementTitle.replace(/\n/ig, '');
        };


        let result = {
          node: {
            tagName: $0.tagName,
            title: generateElementTitle($0),
            page: {
              href: window.location.href,
              origin: window.location.origin,
              pathname: window.location.pathname,
              search: window.location.search
            }
          },
          css: null,
          xpath: null
        };
        
        /*
        * Generate css selector from selected DOM element.
        */
        let generateCSSSelector = function(node, options) {
    
          let cssSelectorTemplate = '{tagname}{attributeholder}{classholder}{idholder}';
          let isBaseElement = false;
    
          if(options.baseCssPath !== null){
            cssSelectorTemplate = cssSelectorTemplate + '>' + options.baseCssPath;
          } else {
            isBaseElement = true;
          }
    
          if(node.nodeName !== undefined && node.nodeName !== ''){
            cssSelectorTemplate = cssSelectorTemplate.replace(/{tagname}/g,node.nodeName);
          }
    
          if(node.attributes.length > 0){
            let isAllowed = false;
            let allowedAttributeCount = 0;
    
            if (isBaseElement && options.attribute.useAttributes.baseElement) {
              isAllowed = true;
              allowedAttributeCount = options.attribute.allowedAttributeCount.baseElement;
            } else if (!isBaseElement && options.attribute.useAttributes.parentElement) {
              isAllowed = true;
              allowedAttributeCount = options.attribute.allowedAttributeCount.parentElement;
            }
    
            if (isAllowed) {
              let validAttributes = options.attribute.allowedAttributes;
              let genAttributes = '';
              let validAttributeIndex;
    
              let hasAttributeCount = 0;
              validAttributes.some(function (attribute) {
                if(node.hasAttribute(attribute) && node.getAttribute(attribute) !== ''){
                  genAttributes += ("[" + attribute + "='" + node.getAttribute(attribute) + "']");
                  hasAttributeCount++;
                }
                if (allowedAttributeCount === hasAttributeCount) {
                  return true;
                }
                return false;
              });
              cssSelectorTemplate = cssSelectorTemplate.replace(/{attributeholder}/g,genAttributes);  
            }
          }
    
          if(node.className !== undefined && node.className !== ''){
            let genClasses = '';
            let classList = node.className.split(" ");
            let classListIndex;
            for(classListIndex in classList){
              genClasses += ('.' + classList[classListIndex]);
            }
            cssSelectorTemplate = cssSelectorTemplate.replace(/{classholder}/g,genClasses);
          }
    
          if(node.id !== undefined && node.id !== ''){
            cssSelectorTemplate = cssSelectorTemplate.replace(/{idholder}/g,'#'+node.id);
          }
    
          if(options.depth !== 0 && cssSelectorTemplate.indexOf('{classholder}') !== -1 && cssSelectorTemplate.indexOf('{idholder}') !== -1){
            return generateCSSSelector(node.parentNode, {
              baseCssPath: cssSelectorTemplate.replace(/{\w+}/g,''),
              depth: options.depth - 1,
              attribute: options.attribute
            });
          } else {
            return cssSelectorTemplate.replace(/{\w+}/g,'');
          }
    
        };

        /*
        * Generate css selector from selected DOM element.
        */
        let generateXPath = function(node, options) {
          return null; // TODO: Need to implement generate expath mechanisum
        };

        result.css = generateCSSSelector($0, {
          baseCssPath: null,
          depth: 3,
          attribute: {
            useAttributes: {
              baseElement: true,
              parentElement: true
            },
            allowedAttributes: ['name','type','value', 'label', 'src', 'href', 'placeholder', 'title', 'data'],
            allowedAttributeCount: {
              baseElement: 3,
              parentElement: 3
            }
          }
        });

        result.xpath = generateXPath();
        
        return result;
    
        /*return {
          parentTagName: $0.parentNode.nodeName,
          tagName: $0.nodeName
        };*/
      }
    
    
    
      let run = '(' + extractNodeData.toString() + ')()';
    
      chrome.devtools.inspectedWindow.eval(run, function(result) {
        $rootScope.$broadcast('element-broadcaster', { result: result });
      });
    };
    
    chrome.devtools.panels.elements.onSelectionChanged.addListener(updateSelectedElement);
  };

  this.init = function () {
    console.log('---------------LOARDED.............');
    console.log('>>>>>', md5('00000000000000'));
    console.log('--------->', _);
    this.bindElementListener();
  };

  this.init();

});

chrome.storage.sync.set({'foo': 'Yoman', 'bar': 'hi'}, function(result, error) {
  console.log('Settings saved');
});

// Read it using the storage API
chrome.storage.sync.get(['foo', 'bar'], function(items) {
  console.log('Settings retrieved', items);
});