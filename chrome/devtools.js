/* global chrome */
'use strict';

chrome.devtools.panels.elements.createSidebarPane(
    "PathFinder",
    function (sidebar) {
        sidebar.setPage('chrome/panel.html');
    }
);