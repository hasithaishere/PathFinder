app.config(function($mdIconProvider) {
  $mdIconProvider
    .iconSet('communication', 'img/icons/sets/communication-icons.svg', 24)
    .iconSet('device', 'img/icons/sets/device-icons.svg', 24);
})
.controller('workbenchCtrl', function($scope, $timeout, $mdSidenav, $log, clipboard, inscpectionService) {
    $scope.imagePaths = {
      css: 'img/icons/css.svg',
      xpath: 'img/icons/xpath.svg'
    };

    $scope.configInfo = {
      allowedType : 'css'
    };

    $scope.doSecondaryAction = function (value) {
      if (!clipboard.supported) {
        console.log('Sorry, copy to clipboard is not supported');
      } else {
        clipboard.copyText(value.css);
      }
    };
    
    $scope.projectName = 'Gmail';

    $scope.inspectedElements = [];

    $scope.$on('element-broadcaster', function(event, args) {
      $scope.inspectedElements.push(args.result);
      $scope.$apply();
    });

    $scope.toggleRight = buildToggler('right');
    $scope.isOpenRight = function(){
      return $mdSidenav('right').isOpen();
    };

    function buildToggler(navID) {
      return function() {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      };
    }

}).controller('RightCtrl', function ($scope, $timeout, $mdSidenav, $log) {
  $scope.close = function () {
    // Component lookup should always be available since we are not using `ng-if`
    $mdSidenav('right').close()
      .then(function () {
        $log.debug("close RIGHT is done");
      });
  };
});
