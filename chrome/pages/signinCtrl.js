app.controller('signinCtrl', function($scope, $location) {
    $scope.user = {
      name: 'John Doe',
      email: '',
      phone: '',
      address: 'Mountain View, CA',
      donation: 19.99
    };

    $scope.signIn = function () {
		$location.path('/workbench');
	};
});
